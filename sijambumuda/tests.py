from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

# Create your tests here.
class SiJambuMudaUnitTest(TestCase):
	def test_index_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_using_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_index_url_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)