from django.urls import path
from django.conf.urls import url
from .views import *

urlpatterns = [
    path('', login, name='login'),
    url(r'^logout/', logout, name='logout'),
]
