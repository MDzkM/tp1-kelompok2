from django.shortcuts import render
from django.urls import reverse
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User
from .models import *
from .forms import *
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout


# Create your views here.
@csrf_exempt
def login(request):
    if (request.method == "POST"):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = User.objects.filter(username = username).exists()
        if user:
            is_authenticate = authenticate(username = username, password = password)
            if is_authenticate:
                auth_login(request, is_authenticate)
                return HttpResponseRedirect(reverse('home:index'))
            else:
                print('error_password')
        else:
            print('error_user')

    return render(request, "login.html")

@login_required
def logout(request):
    auth_logout(request)
    response = HttpResponseRedirect(reverse('home:index'))
    return response