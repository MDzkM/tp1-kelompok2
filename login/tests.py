from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
from datetime import datetime

# Create your tests here.

class BorrowingFormTest(TestCase):

    def test_landing_page_request(self):
        self.response = Client().get('/login/')
        self.assertEqual(self.response.status_code, 200)

    def test_url_name(self):
        self.found = resolve('/login/')
        self.assertEqual(self.found.url_name, 'login')

    def test_redirection_url(self):
        self.found = resolve('/login/')
        self.assertEqual(self.found.func, login)

    def test_form_is_exist(self):
        self.response = Client().get('/login/')
        self.assertIn('</form>', self.response.content.decode())
        self.assertIn('<input id="id_username" name="username" type="text" class="form-control">', self.response.content.decode())
        self.assertIn('<input id="id_password" name="password" type="password" class="form-control">', self.response.content.decode())
        self.assertIn('<input type="submit" value="Login" class="btn btn-primary pull-right"/>', self.response.content.decode())
