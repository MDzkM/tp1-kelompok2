from django.db import models
from datetime import datetime

# Create your models here.
class BorrowedBook(models.Model):
    username = models.CharField(max_length=100)
    email = models.EmailField(max_length=70)
    bookNumber = models.IntegerField()
    dateTime = models.DateTimeField(default=datetime.now, blank=True)
