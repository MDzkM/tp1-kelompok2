from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from .views import *
from .models import *
from .forms import *
from datetime import datetime
from pendaftaran.models import *
from data_buku.models import *
import tempfile

# Create your tests here.

class BorrowingFormTest(TestCase):

    def test_landing_page_request(self):
        self.response = Client().get('/formpeminjaman/')
        self.assertEqual(self.response.status_code, 200)

    def test_url_name(self):
        self.found = resolve('/formpeminjaman/')
        self.assertEqual(self.found.url_name, 'formpeminjaman')

    def test_redirection_url(self):
        self.found = resolve('/formpeminjaman/')
        self.assertEqual(self.found.func, formpeminjaman)

    def test_template_is_exist(self):
        self.response = Client().get('/formpeminjaman/')
        self.assertTemplateUsed(self.response, 'formpeminjaman.html')

    #def test_borrowed_books(self):
        #self.before = BorrowedBook.objects.all().count()
        #BorrowedBook.objects.create(username='foobar', email='foobar@gmail.com', bookNumber='1', dateTime=str(datetime.now()))
        #self.after = BorrowedBook.objects.all().count()
        #self.assertNotEqual(self.before, self.after)

    def test_form_is_exist(self):
        self.response = Client().get('/formpeminjaman/')
        self.assertIn('Form Peminjaman', self.response.content.decode())

    # def test_pinjam_berhasil_dan_stok_habis(self):
    #     # pinjam berhasil
    #     image = tempfile.NamedTemporaryFile(suffix=".jpg").name
    #     Buku.objects.create(nomor=1, kategori='Horror', judul='IT', pengarang='Stephen King', penerbit='Gramedia', sinopsis='Badut', foto=image, stok=1)
        
    #     self.user = User.objects.create(username='testuser', password='testuser_pass', email='testuser@email.com')
        
    #     Client().login(username='testuser', password='testuser_pass')

    #     self.response = Client().post('/formpeminjaman/', {'bookNumber':1})
    #     self.assertIn('Terima kasih!\n Pesanan Anda akan segera di proses.', self.response.content.decode())

    #     # stok habis

    # def test_pinjam_buku_tidak_ada(self):
    #     pass

    #def test_response_is_posted(self):
        #self.response = Client().post('/', {'username' : 'foobar', 'email' : 'foobar@gmail.com', 'bookNumber' : '1', 'dateTime' : str(datetime.now())})
        #self.assertEqual(self.response.status_code, 200)

    #def test_form_is_valid(self):
        #image = tempfile.NamedTemporaryFile(suffix=".jpg").name

        #Pendaftaran.objects.create(nomor_identitas='1234567890', nama_lengkap='FooBar', username='foobar', email='foobar@gmail.com', password='Foobar123_', alamat='Perumahan Kucing, Blok C/AT, Cimanggis, Depok')

        #Buku.objects.create(nomor='1', kategori='Horror', judul='IT', pengarang='Stephen King', penerbit='Gramedia', sinopsis='Badut', foto=image)

        #self.form = BorrowingForm(data={'username' : 'foobar', 'email' : 'foobar@gmail.com', 'bookNumber' : '1', 'dateTime' : str(datetime.now())})

        #self.assertTrue(self.form.is_valid())

        #self.form = BorrowingForm(data={'username' : 'barfoo', 'email' : 'foobar@gmail.com', 'bookNumber' : '1', 'dateTime' : str(datetime.now())})

        #self.assertFalse(self.form.is_valid())

        #self.form = BorrowingForm(data={'username' : 'foobar', 'email' : 'cat@gmail.com', 'bookNumber' : '1', 'dateTime' : str(datetime.now())})

        #self.assertFalse(self.form.is_valid())

        #self.form = BorrowingForm(data={'username' : 'foobar', 'email' : 'foobar@gmail.com', 'bookNumber' : '2', 'dateTime' : str(datetime.now())})

        #self.assertFalse(self.form.is_valid())

    #def test_post_is_valid(self):
        #image = tempfile.NamedTemporaryFile(suffix=".jpg").name

        #Pendaftaran.objects.create(nomor_identitas='1234567890', nama_lengkap='FooBar', username='foobar', email='foobar@gmail.com', password='Foobar123_', alamat='Perumahan Kucing, Blok C/AT, Cimanggis, Depok')

        #Buku.objects.create(nomor='1', kategori='Horror', judul='IT', pengarang='Stephen King', penerbit='Gramedia', sinopsis='Badut', foto=image)

        #self.response = Client().post('/formpeminjaman/', {'username' : 'foobar', 'email' : 'foobar@gmail.com', 'bookNumber' : '1', 'dateTime' : str(datetime.now())})
        
        #self.assertEqual(self.response.status_code, 302)