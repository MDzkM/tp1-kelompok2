from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib import messages
from django.urls import reverse
from .models import *
from .forms import *
from data_buku.models import Buku

import datetime

# Create your views here.

def formpeminjaman(request):
    if request.method == 'POST':
        # if not request.user.is_authenticated:
        #     return HttpResponseRedirect('')
        # else:
            response = {}
            form = BorrowingForm(request.POST or None)

            if form.is_valid():
                now = datetime.datetime.now()

                response['username'] = request.user.username
                response['email'] = request.user.email
                response['bookNumber'] = form.cleaned_data['bookNumber']
                response['dateTime'] = now

                data = BorrowedBook(
                        username=response['username'],
                        email=response['email'],
                        bookNumber=response['bookNumber'],
                        dateTime=response['dateTime']
                    )
                
                daftar_nomor_buku = list(Buku.objects.all().values('nomor'))

                ada = False

                for sesuatu in daftar_nomor_buku:
                    if sesuatu["nomor"]== response['bookNumber']:
                        buku = Buku.objects.get(nomor=response['bookNumber'])
                        ada = True

                if ada==True:
                    if(buku.stok > 0):
                        buku.stok -= 1
                        data.save()
                        buku.save()
                        messages.info(request, "Terima kasih!\n Pesanan Anda akan segera di proses.")
                    
                    else:
                        messages.info(request, 'Maaf, stok buku yang ingin anda pinjam sudah habis')
                    # daftar_nomor_buku = list(Buku.objects.all().values('nomor','stok'))
                    # for i in range(len(daftar_nomor_buku)):
                    #     if (response['bookNumber'] == int(daftar_nomor_buku[i]["nomor"])):
                    #         if(buku.stok > 0):
                    #             buku.stok -= 1
                    #             data.save()
                    #             buku.save()
                    #             messages.success(request, "Terima kasih!\n Pesanan Anda akan segera di proses.")

                    return HttpResponseRedirect(reverse(formpeminjaman))

                else:
                    messages.info(request, 'Maaf, buku yang ingin anda pinjam tidak tersedia pada perpustakaan kami')
                    return HttpResponseRedirect(reverse(formpeminjaman))
    else:
        form = BorrowingForm()

    return render(request, 'formpeminjaman.html', {'form' : form})


# def show_stok(request):
#     data_pinjam = list(BorrowedBook.objects.all().values('username','bookNumber'))
#     daftar_nomor_buku = list(Buku.objects.all().values('nomor'))

#     data_stok = []
#     for i in range(len(daftar_nomor_buku)):
#         data = {}
#         data["bookNumber"] = daftar_nomor_buku[i]["nomor"]
#         data["stok"] = 3
#         data_stok.append(data)

#     for pinjam in data_pinjam:
#         for i in range(len(data_stok)):
#             if (int(pinjam["bookNumber"]) == int(data_stok[i]["bookNumber"])):
#                 data_stok[i]["stok"] -= 1

#         # for stok in data_stok:
#         #     if pinjam["bookNumber"] == stok["bookNumber"]:
#         #         stok["stok"] -= 1

#     return JsonResponse({'subsdata':data_stok})
