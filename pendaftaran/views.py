from django.http import HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse
from django.shortcuts import render
from .models import Pendaftaran
from .forms import Pendaftaran_Form
from django import forms


# Create your views here.
def daftar(request):
    if request.method == 'POST':
        iniForm = Pendaftaran_Form(request.POST)
        if iniForm.is_valid():
                iniForm.save()
                messages.success(request,"Terima Kasih Anda Telah Terdaftar")
                return HttpResponseRedirect(reverse(daftar))
            
    else :
        iniForm = Pendaftaran_Form()

    dataBase = Pendaftaran.objects.all()
    content = {'forms':iniForm, 'data': dataBase}
    return render(request, 'pendaftaran.html', content)

