# Generated by Django 2.1.1 on 2019-03-21 11:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pendaftaran', '0002_auto_20190321_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pendaftaran',
            name='nomor_identitas',
            field=models.CharField(max_length=100),
        ),
    ]
