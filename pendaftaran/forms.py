from django import forms
from .models import Pendaftaran

# class Email_Form(forms.ModelForm):
#     class Meta:
#         model = Pendaftaran
#         fields = ["email"]
#         def clean_email(self):
#             # Get the email
#             email = self.cleaned_data.get('email')

#             # Check to see if any users already exist with this email as a username.
#             try:
#                 match = User.objects.get(email=email)
#             except User.DoesNotExist:
#                 # Unable to find a user, this is fine
#                 return email

#             # A user was found with this as a username, raise an error.
#             raise forms.ValidationError('This email address is exist.')

class Pendaftaran_Form(forms.ModelForm):
    password = forms.CharField(label='Password:', widget=forms.PasswordInput(attrs={'class':'form-control'}), required=True)
    nomor_identitas = forms.CharField(label='Nomor Identitas:', widget=forms.TextInput(
        attrs={'placeholder': 'Kartu Pelajar/KTP/SIM/Paspor', 'class': 'form-control'}), required=True)
    nama_lengkap = forms.CharField(label='Nama Lengkap:', widget=forms.TextInput(
        attrs={ 'class': 'form-control'}), required=True)
    username = forms.CharField(label='Username:', widget=forms.TextInput(
        attrs={ 'class': 'form-control'}), required=True)
    alamat = forms.CharField(label='Alamat Rumah:', widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Email:', widget=forms.TextInput(
        attrs={'class': 'form-control'}), required=True)

    # nomor_identitas = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder' : 'Kartu Pelajar/KTP/SIM/Paspor', 'class':'nomor'}))
    class Meta:
        model = Pendaftaran
        fields = ["nomor_identitas", "nama_lengkap", "username",'email', "password", "alamat", ]
        # def clean_email(self):
        #     # Get the email
        #     email = self.cleaned_data.get('email')

        #     # Check to see if any users already exist with this email as a username.
        #     try:
        #         match = User.objects.get(email=email)
        #     except User.DoesNotExist:
        #         # Unable to find a user, this is fine
        #         return email

        #     # A user was found with this as a username, raise an error.
        #     raise forms.ValidationError('This email address is exist.')