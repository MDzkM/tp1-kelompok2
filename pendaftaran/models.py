from django.db import models

# Create your models here.
class Pendaftaran(models.Model):
    nomor_identitas = models.CharField(max_length=100)
    nama_lengkap = models.CharField(max_length=100)
    username = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=100)
    alamat = models.CharField(max_length=200)

    def __str__(self):              
        return self.username