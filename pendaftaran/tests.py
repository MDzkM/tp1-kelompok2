from django.test import TestCase, Client
from django.urls import resolve
from .views import daftar
from .models import Pendaftaran
from .forms import Pendaftaran_Form
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# # from django.contrib.staticfiles.testing import LiveServerTestCase
# import unittest


# Create your tests here.
class landingTest(TestCase):
    def test_url_landing_is_exist(self):
        response = Client().get('/pendaftaran/')
        self.assertEqual(response.status_code, 200, "wrong URL")

    def test_using_daftar_func(self):
        found  = resolve('/pendaftaran/')
        self.assertEqual(found.func, daftar)

    def test_model_Message(self):
        # Creating a new activity
        activity = Pendaftaran.objects.create(email='rekapaskaendaginting@gmail.com', password = 'qwerty', alamat =  'asdasd', username = 'reka', nomor_identitas = '123', nama_lengkap = 'mama')
        representation = activity.__str__()
        # Retrieving all available activity
        counting_all_available_Message = Pendaftaran.objects.all().count()
        self.assertEqual(counting_all_available_Message, 1)
        self.assertEqual('reka', representation)
    
    def test_email_exist(self):
        activity = Pendaftaran.objects.create(email='rekapaskaendaginting@gmail.com', password = 'qwerty', alamat =  'asdasd', username = 'reka', nomor_identitas = '123', nama_lengkap = 'mama')
        # push = Email_Form('/' ,{'email':'rekapaskaendaginting@gmail.com','nomor_identitas': '123','nama_lengkap': 'mama','password': 'qwerty','alamat': 'asdasd', 'username': 'reka'})
        response_post = Pendaftaran_Form(data={'email':'rekapaskaendaginting@gmail.com', 'nama_lengkap' : 'qweqwe', 'nomor_identitas' : '123123', 'alamat':'asda','password':'asda','username':'qweqw'})
        counting_all_available_Message = Pendaftaran.objects.all().count()
        self.assertEqual(counting_all_available_Message, 1)
        # response = Client().get('/pendaftaran/')
        # html_response = response.content.decode('utf8')
        # self.assertIn('This email address is exist.', html_response)
    
    # def test_form_validation_for_blank_items(self):
    # form = Pendaftaran_Form(data={'email': 'rekapaskaendaginting@gmail.com'})
    # self.assertTrue(form.is_valid())


    def test_form_validation_for_blank_items(self):
        form = Pendaftaran_Form(data={'email': ''})
        self.assertFalse(form.is_valid())
    
    def test_form_validation_for_right_item(self):
        form =Client().post('/pendaftaran/',{'email': 'reka@gmail.com', 'nama_lengkap' : 'qweqwe', 'nomor_identitas' : '123123', 'alamat':'asda','password':'asda','username':'qweqw'})
        # self.assertIn('qweqwe', form.content.decode(), form.content.decode)
        # response = Client().get('/pendaftaran/')
        # html_response = response.content.decode('utf8')
        # self.assertIn("Terima Kasih Anda Telah Terdaftar", html_response, "gaada")
        test = Pendaftaran.objects.get(email="reka@gmail.com").email
        self.assertEqual(test, "reka@gmail.com")

    def test_Form_Pendaftaran(self):
        response = Client().get('/pendaftaran/')
        html_response = response.content.decode('utf8')
        self.assertIn("Form Pendaftaran", html_response)
        
# class FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(FunctionalTest, self).tearDown()

#     def test_input_todo(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/')
#         # find the form element
#         status = selenium.find_element_by_id('id_status')

#         submit = selenium.find_element_by_id('id_submit')

#         # Fill the form with data
#         status.send_keys('Mengerjakan Lab PPW')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#         #check
#         assert 'Mengerjakan Lab PPW' in selenium.page_source
#     def test_profil(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/')
#         # find the form element
#         profil = selenium.find_element_by_id('id_profil')
        
#         # submitting the form
#         profil.send_keys(Keys.RETURN)

#         nama = selenium.find_element_by_tag_name('h1').text
#         self.assertEqual("Reka Paska Enda", nama)

# if __name__ == '__main__':#
#     unittest.main(warnings='ignore')
