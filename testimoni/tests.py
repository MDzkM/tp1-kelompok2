import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

class TestimoniUnitTest(TestCase):
	def test_testimoni_template(self):	
		response = Client().get('/testimoni/')
		self.assertTemplateUsed(response, 'testimoni.html')

	def test_testimoni_page_content(self):
		request = HttpRequest()
		response = testimoni(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Testimoni', html_response)

	def test_create_testimoni_models(self):
		cnt_prev = Testimoni.objects.all().count()
		Testimoni.objects.create(testimoni='Test')
		cnt = Testimoni.objects.all().count()
		self.assertEqual(cnt, cnt_prev+1)

	def test_data_in_form_is_displayed(self):
		testimoni = "Test"
		response = Client().post('/testimoni/', {'testimoni': testimoni})
		self.assertIn(testimoni, response.content.decode())

	def test_testimoni_url_is_exist(self):
		response = Client().get('/testimoni/show')
		self.assertEqual(response.status_code, 200)

	def test_testimoni_function(self):
		found = resolve('/testimoni/show')
		self.assertEqual(found.func, show)