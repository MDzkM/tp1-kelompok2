$(document).ready(function() {
	notebook_data();
 	$("#submit_button").click(function(){
 		notebook_data();
 	});
});

function notebook_data(){
	$.ajax({
		url:"/testimoni/show",
		type:"GET",
		dataType:"json",
		success: function(result){
			for (var itr = 0; itr < result.length; itr++){
				$("#isi").append("<table>");
				$("#isi").append("<tr>");
				$("#isi").append("<th>No : " + (itr+1) + "</th>");
				$("#isi").append("</tr>");
				$("#isi").append("</table>");
				$("#isi").append("<table style='width: 100%; margin-bottom: 20px;'");
				$("#isi").append("<tr>");
				$("#isi").append("<td style='width: 15%; margin: 0px; padding: 0px;'>Testimoni from " + result[itr]["user"] + "</td>");
				$("#isi").append("<td style='width: 80%; margin: 0px; padding: 0px;'>: " + result[itr]["testimoni"] + "</td>");
				$("#isi").append("</tr>");
				$("#isi").append("</table>");
			}
		}
	});
}