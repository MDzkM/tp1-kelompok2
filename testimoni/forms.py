from django import forms

class Testimoni_Form(forms.Form):
    testimoni = forms.CharField(label='Testimoni ', max_length=300, widget=forms.Textarea(attrs={'cols':80, 'rows':5}))