from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse

from .models import Testimoni

from .forms import Testimoni_Form

def testimoni(request):
	form = Testimoni_Form(request.POST or None)
	response = {}
	if (request.method == 'POST' and form.is_valid()):
		response['testimoni'] = form.cleaned_data['testimoni']
		response['user'] = request.user.username
		testimoni = Testimoni(testimoni=response['testimoni'], user=response['user'])
		testimoni.save()
	response = {'testimoni_form': form}
	return render(request, 'testimoni.html', response)

def show(request):
	lst = []
	item = Testimoni.objects.all().values()
	for i in item:
		lst.append(i)
	return JsonResponse(lst, safe=False)