from django.test import TestCase, Client
from django.urls import resolve
from .views import riwayat, data
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest
# from django.contrib.staticfiles.testing import LiveServerTestCase

# Create your tests here.
class Riwayat_test(TestCase):
    # def test_url_riwayat_is_exist(self):
    #     response = Client().get('/riwayat/')
    #     self.assertEqual(response.status_code, 200, "wrong URL")

    def test_url_data_is_exist(self):
        response = Client().get('/riwayat/data/')
        self.assertEqual(response.status_code, 200, "wrong URL")

    def test_using_riwayat_func(self):
        found  = resolve('/riwayat/')
        self.assertEqual(found.func, riwayat)
    
    # def test_Riwayat(self):
        # response= Client().get('/riwayat/')
        # html_response = response.content.decode('utf8')
        # self.assertIn("Riwayat", html_response)

    # def test_using_data_func(self):
    #     found  = resolve('/data/')
    #     self.assertEqual(found.func, data)