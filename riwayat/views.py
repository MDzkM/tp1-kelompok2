from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse
import random
import requests
from formpeminjaman.models import BorrowedBook
from data_buku.models import Buku


# Create your views here.
def riwayat(request):
    if request.user.is_authenticated:
        return render(request,'riwayat.html')
    else:
        return HttpResponseRedirect('/')
def data(request):
    lst = []
    item = BorrowedBook.objects.all().values()
    buku = Buku.objects.all().values()
    # dataKu = item.json()
    if request.user.is_authenticated:
        email = request.user.email
        for i in item:
            if (email == i["email"]):
                for j in buku:
                    if (i["bookNumber"]) == j["nomor"]:
                        i["judul"] = j["judul"]
                        lst.append(i)
    
    return JsonResponse(lst, safe=False)
    
