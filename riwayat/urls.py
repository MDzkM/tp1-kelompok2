from django.urls import path
from .views import riwayat, data

urlpatterns = [
    path('', riwayat, name='riwayat'),
    path('data/', data, name='data')
]