from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.test import Client

from formpeminjaman.models import *
from data_buku.models import *

from .views import *
import datetime

# Create your tests here.

class DataPeminjamanUnitTest(TestCase):
    # test halaman tujuan
    def test_url_data_peminjaman_is_exist(self):
        response = Client().get('/data-peminjaman/')
        self.assertEqual(response.status_code, 200, "Berhasil ke halaman")

    def test_data_peminjaman_using_data_func(self):
        found = resolve('/data-peminjaman/')
        self.assertEqual(found.func, data_peminjaman, "Menggunakan fungsi yang sesuai")

    def test_data_url_using_data_template(self):
        response = Client().get('/data-peminjaman/')
        self.assertTemplateUsed(response, 'data-peminjaman.html', "Template sesuai")

    # test judul halaman
    def test_judul_data_peminjaman(self):
        response = Client().post('/data-peminjaman/', {})
        self.assertIn("Data Peminjaman Buku", response.content.decode())

    # test data peminjaman

    def test_data_peminjaman_is_shown(self):

        self.waktu = str(datetime.datetime.now())
        self.before = BorrowedBook.objects.all().count()
        BorrowedBook.objects.create(username='foobar', email='foobar@email.com', bookNumber='001', dateTime=self.waktu)
        self.after = BorrowedBook.objects.all().count()
        self.assertNotEqual(self.before, self.after)

        self.sebelum = Buku.objects.all().count()
        Buku.objects.create(nomor=1, kategori='comedy', judul='buku', pengarang='someone', penerbit='terbit', sinopsis='text', foto='foto', stok=3)
        self.sesudah = Buku.objects.all().count()
        self.assertNotEqual(self.sebelum, self.sesudah)

        response = Client().get('/data-peminjaman/')
        self.assertIn('foobar', response.content.decode())
        self.assertIn('foobar@email.com', response.content.decode())
        self.assertIn('1', response.content.decode())

    def test_judul_buku(self):
        BorrowedBook.objects.create(username='foobar', email='foobar@email.com', bookNumber='001', dateTime=str(datetime.datetime.now()))
        Buku.objects.create(nomor=1, kategori='comedy', judul='buku', pengarang='someone', penerbit='terbit', sinopsis='text', foto='foto', stok=3)
        list_peminjaman = BorrowedBook.objects.all().values()
        buku = Buku.objects.all().values()
        for pinjam in list_peminjaman:
            for barang in buku:
                self.assertEqual(barang['nomor'], (pinjam['bookNumber']))