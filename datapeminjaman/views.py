
from django.shortcuts import render
from django.http import HttpResponseRedirect

from formpeminjaman.models import *
from data_buku.models import *

# Create your views here.
def data_peminjaman(request):
	list_peminjaman = BorrowedBook.objects.all().values()
	buku = Buku.objects.all().values()
	for pinjam in list_peminjaman:
		for barang in buku:
			if barang['nomor'] == str(pinjam['bookNumber']):
				pinjam['judul'] = str(barang['judul'])
	response = {"list_peminjaman":list_peminjaman}
	return render(request, 'data-peminjaman.html', response)