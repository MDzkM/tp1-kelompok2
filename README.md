# tp1-kelompok2

[![pipeline status](https://gitlab.com/MDzkM/tp1-kelompok2/badges/master/pipeline.svg)](https://gitlab.com/MDzkM/tp1-kelompok2/commits/master)
[![coverage report](https://gitlab.com/MDzkM/tp1-kelompok2/badges/master/coverage.svg)](https://gitlab.com/MDzkM/tp1-kelompok2/commits/master)

Project for first group assignment in PPW class.

Anggota Kelompok:
1. I Made Krisna Dwitama - 1806133881
2. Muhammad Dzikra Muzaki - 1806141334
3. Nurul Srianda Putri  - 1806205602
4. Reka Paska Enda - 1806186793

Herokuapp Link: http://sijambumuda2.herokuapp.com/

<---Pembagian Tugas Pada TP 1--->

Fitur website:

 -> Registrasi User (Dikerjakan oleh Reka Paska Enda)
 
    Mendaftarkan user sebagai pengguna yang dapat meminjam.
    
 -> Database Buku (Dikerjakan oleh I Made Krisna Dwitama)
 
    User dapat melihat buku apa saja yang tersedia untuk dipinjam.
    
 -> Peminjaman Buku (Dikerjakan oleh Muhammad Dzikra Muzaki)
 
    User dapat mengisi form untuk meminjam buku yang tersedia pada database menggunakan akun yang telah dibuat.
    
 -> Daftar Peminjaman Buku (Dikerjakan oleh Nurul Srianda Putri)
 
    User dapat melihat daftar peminjaman buku yang sedang berlangsung maupun yang akan dipinjam.
    
<---Pembagian Tugas Pada TP 2--->

Fitur website:

 -> Login Page dan Validasi (Dikerjakan oleh Muhammad Dzikra Muzaki)
 
 -> Penambahan Fitur dalam Form Peminjaman dan Stok Buku (Dikerjakan oleh Nurul Srianda Putri)
 
 -> About Page dan Testimoni (Dikerjakan oleh I Made Krisna Dwitama)
 
 -> Daftar Buku yang Dipinjam User dan Profil (Dikerjakan oleh Reka Paska Enda)
 