from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Buku

def data_buku(request):
	list_buku = Buku.objects.all().values()
	response = {'list_buku': list_buku}
	return render(request, 'data_buku.html', response)