from django.apps import AppConfig


class DataBukuConfig(AppConfig):
    name = 'data_buku'
