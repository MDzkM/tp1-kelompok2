from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

# Create your tests here.
class Story6UnitTest(TestCase):
	def test_data_buku_url_is_exist(self):
		response = Client().get('/data_buku/')
		self.assertEqual(response.status_code, 200)

	def test_data_buku_template(self):
		response = Client().get('/data_buku/')
		self.assertTemplateUsed(response, 'data_buku.html')

	def test_data_buku_function(self):
		found = resolve('/data_buku/')
		self.assertEqual(found.func, data_buku)

	def test_data_buku_page_content(self):
		request = HttpRequest()
		response = data_buku(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Daftar', html_response)
		self.assertIn('Nomor Buku', html_response)
		self.assertIn('Kategori Genre', html_response)
		self.assertIn('Judul Buku', html_response)
		self.assertIn('Nama Pengarang', html_response)
		self.assertIn('Nama Penerbit', html_response)
		self.assertIn('Sinopsis Buku', html_response)
		self.assertIn('Foto Cover', html_response)

	def test_create_buku_models(self):
		cnt_prev = Buku.objects.all().count()
		Buku.objects.create(nomor=0, kategori="test1", judul="test2", pengarang="test3",
			penerbit="test4", sinopsis="test5", foto="test6", stok=3)
		cnt = Buku.objects.all().count()
		request = HttpRequest()
		response = data_buku(request)
		html_response = response.content.decode('utf8')
		self.assertIn('0', html_response)
		self.assertIn('test1', html_response)
		self.assertIn('test2', html_response)
		self.assertIn('test3', html_response)
		self.assertIn('test4', html_response)
		self.assertIn('test5', html_response)
		self.assertIn('test6', html_response)
		self.assertEqual(cnt, cnt_prev+1)