from django.db import models

class Buku(models.Model):
	nomor = models.IntegerField()
	kategori = models.CharField(max_length=100)
	judul = models.CharField(max_length=100)
	pengarang = models.CharField(max_length=100)
	penerbit = models.CharField(max_length=100)
	sinopsis = models.TextField()
	foto = models.TextField()
	stok = models.IntegerField()